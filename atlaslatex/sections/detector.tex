% Footnote with ATLAS coordinate system
\newcommand{\AtlasCoordFootnote}{%
ATLAS uses a right-handed coordinate system with its origin at the nominal interaction point (IP) in the centre of the detector and the \(z\)-axis along the beam pipe.
The \(x\)-axis points from the IP to the centre of the LHC ring, and the \(y\)-axis points upwards.
Cylindrical coordinates \((r,\phi)\) are used in the transverse plane, \(\phi\) being the azimuthal angle around the \(z\)-axis. The pseudorapidity is defined in terms of the polar angle \(\theta\) as \(\eta = -\ln \tan(\theta/2)\). Angular distance is measured in units of \(\Delta R \equiv \sqrt{(\Delta\eta)^{2} + (\Delta\phi)^{2}}\).}

The ATLAS experiment~\cite{PERF-2007-01} at the LHC is a multipurpose particle detector
with a forward--backward symmetric cylindrical geometry and a near \(4\pi\) coverage in 
solid angle.\footnote{\AtlasCoordFootnote}
It consists of an inner tracking detector, electromagnetic and hadron calorimeters and a muon spectrometer. The inner tracking detector covers the pseudorapidity range \(|\eta| < 2.5\). The reconstruction of charged particle trajectories is performed in three sub-detectors: a high granularity silicon Pixel Detector, the silicon microstrip Semi-Conductor Tracker detector (SCT) and the Transition Radiation Tracker (TRT). These are contained in a thin superconducting solenoid providing a \SI{2}{\tesla} axial magnetic field.

Of particular importance for this study is the Pixel Detector. Its barrel section is equipped with four concentrical layers, Insertable B-Layer (IBL)~\cite{ATLAS-TDR-19}, B-Layer (BL), Layer 1 (L1) and Layer 2 (L2), located at average radial positions of 3.3, 5.5, 9 and 12.5 \si{\cm}, respectively. The transverse $r-\phi$ coordinate of charged particle passage is measured along the pixel rows  ($X$ in the detector local reference frame), on a 50~\si{\micron} pitch, the longitudinal $z$ coordinate along the columns ($Y$ in the detector local reference frame), on a 250~\si{\micron} pitch in the IBL and 400~\si{\micron} pitch in the other layers. The detector staves are arranged with overlaps between adjacent modules in $\phi$. Two endcap sections complement the barrel part and consist of three double disks located at $\pm$50, $\pm$58 and $\pm$65~\si{\cm} from the nominal beam interaction point. The IBL was installed at the beginning of Run~2 in 2015, while the other layers operate since the start of ATLAS data taking at LHC.

IBL modules consist of 200(230)$\mu$m thickness silicon planar(3D) sensor with 2(1) FE-I4 front-end readout chips~\cite{fei4}. The other pixel modules consist of 250$\mu$m silicon sensor with 16 FE-I3 front-end chips~\cite{fei3}.

Each readout cell features an analog block performing signal amplification and discrimination and a digital block computing the Time over Threshold (ToT), which is proportional to the detected charge. This is coded on 4 bits for the FE-I4 and on 8 bits for the FE-I3 chips. The limited dynamic range on FE-I4 is mitigated by a different ToT-to-charge conversion with hits having ToT higher than 14 are stored in the highest ToT bin and receive an overflow tag.

Front-end chips are individually calibrated in scans where the ToT value corresponding to a given amount of injected charge on reference capacitors in the FE chips is recorded. The calibration tuning points for Run~3 are 12 k$e^-$ at ToT=10 for IBL, 16 k$e^-$ at ToT=18 for B layerand 16 k$e^-$ at ToT=30 for the other layers. The ToT-to-charge curves are then fit with a three parameter function for each front-end chip and these are used to convert the pixel ToT response into units of collected charge. The IBL charge calibration is corrected using the response to an Am$^{241}$ source scan performed before installation and a suitable correction is applied to the simulated IBL charge.

Electromagnetic (EM) energy depositions are measured with high granularity in the lead/liquid-argon (LAr) sampling calorimeters, particularly in the azimuthal angular region corresponding to the ID coverage. A steel/scintillator-tile hadron calorimeter covers the central pseudorapidity range (\(|\eta| < 1.7\)). The endcap and forward regions are instrumented with LAr calorimeters for both the EM and hadronic energy measurements up to \(|\eta| = 4.9\).

The muon spectrometer surrounds the calorimeters and is based on a system of precision gaseous tracking chambers and fast detectors for triggering inside the magnetic field provided by three large superconducting air-core toroidal magnets with eight coils each.
%The field integral of the toroids ranges between \num{2.0} and \SI{6.0}{\tesla\metre} across most of the detector. 
A two-level trigger system is used to select events and reduce the incoming data rate.
%of \SI{40}{\MHz}.
The first-level trigger is implemented in custom-built hardware and uses a subset of the detector information to accept events up to a rate of \SI{100}{\kHz}. This is followed by a software-based trigger that reduces the accepted event rate to \SI{1}{\kHz} on average depending on the data-taking conditions.
