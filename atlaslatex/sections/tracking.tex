
A total of XXX tracks is selected in the 900~GeV data from the sample described in Section~\ref{sec:data}.

\subsection{Inner Detector alignment}

Possible vibrations during the LS2 may have caused to significant movement of the Inner Detector components with respect to their position determined at the end of Run~2. Therefore, an updated description of the detector geometry is needed prior to $pp$ collision data-taking at $\sqrt{s}=13.6$~TeV.  The ATLAS Inner Detector alignment aims at the description of the detector geometry at the highest level of precision possible by means of the minimization of the track-hit residuals \cite{IDTR-2019-05}.  These residuals are computed in the local coordinate system with the local-$X$ coordinate in the direction of the small pixel pitch and local-$Y$ in the orthogonal direction to the local-$X$ axis in the sensor plane.

Reconstructed tracks from first $pp$ collisions at $900$~GeV have been used to quantify the movements of the detector during LS2.  A rotation of the whole IBL of approximately $0.3$ mrad with respect to the position determined in the last LHC fill in 2018 \cite{ATL-PHYS-PUB-2022-028} is observed, corresponding to a movement of the mean of the residual distribution in the IBL local-$X$ coordinate around $10$~$\mu${m}, as shown in Figure~\ref{fig:align_ibl_x}.  This rotation of the IBL is reflected in the track-hit residuals in the local-$X$ projection in the B Layer (see Figure~\ref{fig:align_bl_x}), as reconstructed tracks are systematically biased from the displacement of the IBL, providing npn-zero residuals of opposite sign in the B Layer.  An additional movement of the IBL of around $13$~$\mu${m} along the beam axis (local-$Y$) is also observed as shown in Figure~\ref{fig:align_ibl_y},  reflected also in the track-hit residual along the local-$Y$ coordinate in the B Layer (Figure~\ref{fig:align_bl_y}). No other significant displacement with respect to Run~2 geometry have been observed.

\begin{figure} [!ht]
\centering
\subfloat[\label{fig:align_ibl_x}]{\includegraphics[width=0.4\textwidth]{figures/PIX_IBL_X.pdf}}
\subfloat[\label{fig:align_ibl_y}]{\includegraphics[width=0.4\textwidth]{figures/PIX_IBL_Y.pdf}} \\
\subfloat[\label{fig:align_bl_x}]{\includegraphics[width=0.4\textwidth]{figures/PIX_BLAYER_X.pdf}}
\subfloat[\label{fig:align_bl_y}]{\includegraphics[width=0.4\textwidth]{figures/PIX_BLAYER_Y.pdf}}
\caption{Track-hit residuals in IBL in (a) local-$x$ and (b) local-$y$ coordinates and in B-Layer in (c) local-$x$ and (d) local-$y$ coordinates.}
\label{fig:align_residual}
\end{figure}

These global movements of the IBL have been corrected by the alignment procedure, in which only global movements of the IBL detector are included as degrees-of-freedom for the track-hit residual minimisation.  The alignment procedure resulted in a preliminary description of the detector geometry in preparation for the Run~3 high-energy collision data,  with almost negligible track-hit residuals in the whole Inner Detector, as shown in Figure~\ref{fig:align_residual}. The slightly degraded position resolution with respect to that of Run~2 indicates a still sub-optimal description of the detector geometry, due to the limited number of degrees-of-freedom included in the alignment procedure so far.  This was due to the limited statistics available and the relatively low-$\pT$ of particle tracks in the 900~GeV data.

\subsection{Tracking Results}

Tracking reconstruction is characterised by studying the number of hits in the three ID detectors, Pixel, SCT and TRT, associated to particle tracks and the distributions of the track perigee parameters evaluated at the position of the reconstructed primary vertex.

\begin{figure}[htb]
  \centering
   \begin{tabular}{cc}
    \includegraphics[width=0.475\textwidth]{figures/data22_900GeV.NPix.pdf} &
    \includegraphics[width=0.475\textwidth]{figures/data22_900GeV.NPixVsEta.pdf} \\  
  \end{tabular}
  \caption{Distributions of the number of pixel hits associated to selected particle tracks (left panel) and its average value as a function of the pseudorapdity, $\eta$ of the tracks in data (filled points with error bars) and simulation (continuous line). \label{fig:TrackingNPix}}
\end{figure}

The number of hits on track observed in data and simulation are compared for tracks passing the track selection criteria discussed in Section~\ref{sec:data}. Figure~\ref {fig:TrackingNPix} shows the distributions of pixel hits. Figure~\ref {fig:TrackingNHits} shows the average number of SCT and TRT hits as a function of the tracks pseudorapidity. Data and MC simulation agree well for all the three detectors.

\begin{figure}[htb]
  \centering
   \begin{tabular}{cc}
    \includegraphics[width=0.475\textwidth]{figures/data22_900GeV.NSCTVsEta.pdf} &
    \includegraphics[width=0.475\textwidth]{figures/data22_900GeV.NTRTVsEta.pdf} \\  
  \end{tabular}
  \caption{Distributions of the average number of SCT (left panel) and TRT (right panel) hits associated to selected particle tracks as a function of the pseudorapdity, $\eta$ of the tracks in data (filled points with error bars) and simulation (continuous line). \label{fig:TrackingNHits}}
\end{figure}

The distributions of the track transverse impact parameter, $d_0$, and the distance between track and reconstructed 
primary vertex in the longitudinal plane, $z_0$-$z_0^{vertex}$,  are shown in Figure~\ref{fig:TrackingIP}.
\begin{figure}[htb]
  \centering
   \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/d0_data.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/dZ_data.pdf} \\  
  \end{tabular}
  \caption{The distributions of the track transverse impact parameter, $d_0$ (left), and the distance between track and reconstructed 
            primary vertex in the longitudinal plane, $z_0$-$z_0^{vertex}$ (right).
%Distributions of the distance of closest approach of particle tracks to the reconstructed event primary vertex in the transverse (left) and longitudinal (right) %projection in 900~GeV collisions data for data (filled points with error bars).  
\label{fig:TrackingIP}
  }
\end{figure}


\subsection{Vertexing Results}

For primary vertex reconstruction in Run~3 ATLAS developed a new algorithm based on an Adaptive multi-vertex fitter (AMVF) and a Grid Seed Finder 
for vertex candidate search.  The AMVF was first implemented using a standard ATLAS Run~2 geometrical model \cite{AMVF}, later it was re-implemented in the ACTS framework using a simplified  ACTS based Inner Detector model to improve performance.  The ACTS implementation of the AMVF is a default primary vertex reconstructor for Run~3.
 
\begin{figure}[htb]
  \centering
   \begin{tabular}{ccc}
    \includegraphics[width=0.33\textwidth]{figures/XvPos_data.pdf} &
    \includegraphics[width=0.33\textwidth]{figures/YvPos_data.pdf} &             
    \includegraphics[width=0.33\textwidth]{figures/ZvPos_data.pdf} \\  
  \end{tabular}
  \caption{Distributions of the position of the reconstructed primary vertex in 900~GeV collision data. \label{fig:TrackingPVtx}}
\end{figure}

\begin{figure}[htb]
  \centering
   \begin{tabular}{cc}
    \includegraphics[width=0.49\textwidth]{figures/nTrk_data.pdf} &
    \includegraphics[width=0.49\textwidth]{figures/Chi2Ndf_data.pdf}\\
  \end{tabular}
  \caption{Distributions of the number of tracks included into the primary vertex fit (left) and $\chi^2 / ndf$ of the fit (right). \label{fig:VertexNChi2}}
\end{figure}

\begin{figure}[htb]
  \centering
   \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/d0extrap_data.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/dZextrap_data.pdf} \\  
  \end{tabular}
  \caption{The distributions of the track transverse impact parameter, $d_0$ (left) , and the longitudinal coordinate, $z_0$ (right),  of the track Perigee position,
   calculated with respect to the reconstructed Primary Vertex of the event. \label{fig:TrackExtrapPV}}
\end{figure}

The AMVF extends the functionality of the single vertex fitter by allowing tracks to contribute to multiple vertices with appropriate weights. 
The algorithm processes several overlapped vertices at once, recalculating a track-to-vertex contribution weight for every track together with vertex positions 
in every iteration, until they get stabilised.  This approach is able to privide a good vertex-vertex resolution together 
with a precise single vertex position determination.

The distributions of the position of the reconstructed primary vertex in 900~GeV collision data are shown in Figure~\ref{fig:TrackingPVtx}.
Number of tracks used in the primary vertex fit and $\chi^2 / N_{dof}$ of the fit are shown in Figure~\ref{fig:VertexNChi2}.
The reconstructed track parameters can be extrapolated to the reconstructed Primary Vertex position to validate the precision and unbiasness of the overall data processing procedure.
The corresponding distributions are presented in Figure~\ref{fig:TrackExtrapPV}.

