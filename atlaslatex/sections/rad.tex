Radiation damage affects the response of detectors, in particular those located closest to the interaction region. The ATLAS pixel sensors installed in the innermost layers, report a charge collection efficiency reduced by $\sim$20\% during Run~2. This is expected to further decrease to a factor of more than two, compared to the original response, by the end of Run~3. This requires a careful optimisation for the detector operating conditions and reliable tools to predict the evolution of the sensor performance with fluence and its impact on event reconstruction.

This section presents a short summary of radiation damage effects on the pixel sensors and their modelling in the digitization step of ATLAS radiation damage MC simulation using a dedicated algorithm whose inputs are taken from detailed silicon device simulations.

\subsection{Radiation damage to pixel sensors}
\label{sec:raddamage}

The intense flux of particles resulting from the LHC $pp$ collisions - and their interactions with the detector material - creates dislocation of atoms from lattice sites in the bulk of Silicon detectors~\footnote{The nature and effects of radiation damage to the readout electronics will not be discussed here}.  
The combination of interstitial Silicon and the lattice vacancy is the basic defect induced by radiation. Such basic defect  can combine with other defects or impurities, leading to the creation of cluster of defects. The radiation induced defects create states in the forbidden bandgap of Silicon. These states can charge up, by capturing/emitting carriers. This mechanism  induces important changes in pixel detector operations and performance. In the following a brief description of the basic phenomena is. For more informations, the reader is referred to the reviews in~\cite{MollTNS2018,Dawson:2764325,}.


%A defect is usually characterised by its energy level with respect to the bandgap, its density per unit of irradiation, its possible charge states and the carriers cross sections. 
%The number and characteristics of all the radiation induced defects is still subject of intense research, but a rather solid knowledge of the topic exists, and 
%It is possible to link some of the observed macroscopic changes in detector behaviour with some of these defects.

At macroscopic level the main effects of the accumulation of radiation damage defects are the increase of sensor leakage current, a change of the depletion voltage - due to a change of the electric field profile - and -most important for physics performance - a reduction in signal amplitude. 

Defects can create electron and hole pairs, leading to a linear increase of the leakage current $I$ increase with the radiation damage fluence $\Phi$~\footnote{Fluence $\Phi$, measured in (1~MeV)~n$_{\text{eq}}$/cm$^2$ is a weighted sum of the flux of particles integrated over time, rescaled to the damage produced by neutrons of 1~MeV energy}.
% as: $\Delta I = \alpha \Phi V$,
%\begin{equation}
%\Delta I = \alpha \Phi V
%\label{eq:deltaI}
%\end{equation}
%where $\alpha\sim$~4~$\times10^{-17}$~A/cm and $V$ is the detector volume. 
For example, the IBL sensors showed a current density of several mA's per cm$^3$~\cite{LeakageCurrent2021} at the end of Run2, as shown in Figure~\ref{fig:IBL_ILeak}, to be compared to a fraction of $\mu$A for a non-irradiated module~\cite{IBL_paper}. 

%For example, an IBL planar double sensor after a fluence of
 %$\Phi = 1\times10^{15}$n$_{\rm eq}$/cm$^2$ (close to the end of Run2 integrated fluence) draws about 1.6~mA at room temperature. This requires to operate the detector cold 
 %to avoid potential thermal runaway and the limitations of high voltage power supplies.
 
\begin{figure}[!htbp]
\begin{center}
   \includegraphics[width=0.50\textwidth]{figures/IBL_ILeak_fig_01.pdf}
   \caption{\label{fig:IBL_ILeak} Measured and predicted leakage currents for sensors on the Insertable $B$-layer, both normalized to 0~$\deg$~C for four module groups spanning $|z|<8$~cm, $8<|z|<16$~cm, $16<|z|<24$~cm, and $24<|z|<32$~cm. Modules in the highest $|z|$ region use 3D sensors. For all four predictions, the overall scale normalization is based on a fit to the data across the entire range. Normalization factors are determined per $|z|$ region. The lower panel shows the ratio of the prediction to the data for the innermost module group. Similar MC-to-data trends are observed for the other three $|z|$ regions (from~\cite{LeakageCurrent2021}).}
   %(right) The fluence-to-luminosity conversion factors for the ATLAS Pixel Detector as a function of $z$, compared with the Pythia 8 (A3 tune) +FLUKA and Pythia 8 (A3 tune)+Geant4 predictions. Distances given in parentheses behind layer names correspond to the radius of the sensors with respect to the geometric center of ATLAS~\cite{LeakageCurrentPlots}.}
\end{center}  
\end{figure}
 
As defects charge up, they alter the bulk charge distribution, which implies a change in the electric field in the sensor. As a result, larger and larger bias voltage is needed to deplete the Silicon bulk.
%As more fixed charge is present in the bulk, the voltage required to deplete it increases, and also the electric field profile is affected, as the distribution of charged defects is not uniform across the bulk. 
The bias voltage of IBL planar sensors needed to be raised from 80~V at the beginning of the Run2 to 400~V at its end.
%, as it can be seen in Figure~\ref{fig:vdepl}.
 
 %\begin{figure}[!htbp]
%\begin{center}
 %\subfloat[]{  \includegraphics[width=0.45\textwidth]{figures/IBL_Vdepl.pdf}}
  %\subfloat[]{  \includegraphics[width=0.45\textwidth]{figures/BL_Vdepl.pdf}}
    %\caption{\label{fig:vdepl} Calculated depletion voltage of (a) IBL and (b) B-layer according to the Hamburg model as a function of time from the date of their installation until the end of 2016. Circular points indicate measurements of the depletion voltage using the bias voltage scan method while square points display earlier measurements using cross-talk scans.}
    %\end{center}  
%\end{figure}
 
Defects can trap carriers created by ionising particles, leading to a significant reduction of the signal amplitude. The trapping phenomena is characterised by a trapping time $\tau$ which is measured to be inversely proportional to the fluence $\Phi$: $\tau^{-1}_{e,h} = \beta_{e,h} \Phi$,
%\begin{equation}
%\tau^{-1}_{e,h} = \beta_{e,h} \Phi 
%\label{eq:trapping}
%\end{equation}
where the trapping coefficients~$ \beta$ values are larger for electrons than holes~\cite{RadDamagePaper2019}. 

%\begin{figure}[!htbp]
%\begin{center}
   %\includegraphics[width=0.45\textwidth]{figures/IBL_CCE.pdf}
   %\caption{\label{fig:IBL_CCE} The charge collection efficiency as a function of integrated luminosity for 80 V, 150 V, and 350 V bias voltage. A linear trendline is added to the simulation to guide the eye. The bias voltage was increased during data-taking, so the data points are only available at increasing high-voltage values. The points are normalized to unity for a run near the beginning of Run 2. The uncertainty on the simulation includes variations in the radiation damage model parameters as well as the uncertainty in the luminosity-to-fluence conversion. Vertical uncertainty bars on the data are due to the charge calibration drift. Horizontal error bars on the data points due to the luminosity uncertainty are smaller than the markers. From~\cite{RadDamagePaper2019}.}
   %\end{center}
   %\end{figure}

%{\bf TODO}:Add plots from Run2 performance, like leakage current and charge collection evolution 


\subsection{Radiation damage modeling in ATLAS Monte Carlo simulation}
\label{sec:raddigi}

The reduction of signal amplitude is the most evident effect of radiation damage in pixel sensors. This degradations induces changes to the hit efficiency and spatial resolution and the efficiency for identifying cluster generated by the charge deposition of multiple charged particles.
%that may be induced by the signal charge losses are mitigated.
%The main reason is pixel signals being below threshold, leading to hit inefficiencies and/or errors in cluster position determination, for example when a true multiple-pixel cluster is misreconstructed as a single pixel cluster or a broken cluster. Another effect is a distorsion in cluster shape in long clusters as electrons produced far away from pixel readout are more likely to get trapped; this leads to biases in the determination of clusters positions. The change in the electric field profiles determines also a change in the Lorentz force deflection; during Run1 and 2 it has been observed a drift of the measured Lorentz angle value which made the Monte Carlo simulations no longer compatible with the data. 

Therefore, it is important to model these effects in simulations, and train tracking reconstruction algorithms on radiation-aware MC event samples.  For the start of Run3 it is possible to simulate radiation damage effects in the digitization step of pixels, the so-called ``radiation damage digitizer''.
The induced signal on electrodes by the carriers produced by ionising particles in Geant4~\cite{Agostinelli:2002hh} is calculated using precise electric field, Lorentz angle and weighting potential~\cite{ShockleyPot,Ramo} maps, taking into account carriers trapping and diffusion. Details of their implementation can be found in~Ref.~\cite{RadDamagePaper2019}.
%In the following the main features of the algorithm are briefly discussed later~(\ref{sec:tcad}) the needed inputs will be reviewed.

Most of the inputs to the radiation damage digitizer are taken from detailed TCAD~\footnote{Technology Computer Aided Design} simulations. TCAD tools allow to simulate silicon detector  devices behaviour by numerically solving the Poisson, carrier transport and continuity equations on a grid of points.  In order to model the modifications due to radiation damage a few (2 to 5 typically) deep defects are added to the basic device simulation. For example, for IBL and Pixel planar sensors a model that includes two defects is used~\cite{CHIOCHIA2006}. The contribution of these effective defects is taken into account in all the equations and leads to the prediction of the electric field distribution after irradiation. Examples of electric field profile predictions at different fluences are shown in Figure~\ref{fig:planar_field}.
 \begin{figure}[!htbp]
\begin{center}
   \includegraphics[width=0.50\textwidth]{figures/planar_field.pdf}
    \caption{\label{fig:planar_field}The simulated electric field magnitude in the $z$ direction along the bulk depth, averaged over the $x$ and $y$ coordinates for an ATLAS IBL sensor biased at 150~V for various fluences (adapted from~\cite{RadDamagePaper2019}).}
   \end{center}
   \end{figure}   
 
 
 This predicted electric field is used to calculate the expected time spent by charge carriers to reach the collecting electrode, via the carrier-mobility relation. Electric field and mobility are also used to calculate the expected average Lorentz angle for a carrier produced at a certain position and trapped at a later one. All these quantities are precalculated and stored as numerical maps. The weighting potential map of the detector, which is used to determine the final induced signal on all pixels, is also precalculated using  TCAD tools.

%Figure~\ref{fig:digi_algo} presents a flowchart of the physics models included in the digitization model~\cite{RadDamagePaper2019}. 
  
% \begin{figure}[!htbp]
%\begin{center}
  % \includegraphics[width=0.45\textwidth]{figures/digi_algo.pdf}
    %\caption{\label{fig:digi_algo}A flowchart illustrating the components of the radiation damage digitizer. The digitizer takes advantage of pre-computation to re-use as many calculations as possible. For example, many inputs are the same for a given condition (temperature, bias voltage, fluence). The Ramo potential only depends on the sensor geometry and the quantities in dashed boxes further depend only on the condition information. The output of the algorithm described in this paper is an induced charge on the primary electrode and the neighbours, which is then converted into a ToT by the existing software. Adapted from~\cite{RadDamagePaper2019}.}
   %\end{center}
   %\end{figure}   
 
 Upon initialization, the digitizer receives global information about the detector geometry (pixel size and type, tilt angle) and conditions, including the sensor bias voltage, operating temperature, and fluence. For the calculation of individual charge deposits within the pixel sensor, the digitizer takes as input by Geant4 the magnitude and location of energy deposited by a charged particle, and outputs a digitized encoding of the measured charge.
 %A TCAD tool is used to model the electric field, including radiation damage effects. The Lorentz angle deflection and the total drift time to reach the collecting electrode is calculated using the electric field prediction.
 In order to speed up the simulation, charge carriers are grouped and drift / diffusion is applied collectively to this set. For each charge group, a fluence-dependent time-to-trap  is randomly generated and compared with the drift time. If the drift time is longer than the time-to-trap, the charge group is declared trapped, and its trapping position is calculated. Since moving charges induce a current in the collecting electrode, a signal is induced on the electrodes also from trapped charges during their drift. This induced charge also applies to neighbouring pixels, which contributes to charge sharing. The induced charge is calculated from the initial and trapped positions using a weighting potential map. The total induced charge is then converted into a Time-over-Threshold (ToT)~\cite{AtlasPixels} that is used by cluster and track reconstruction tools.
 
 This procedure has proven to provide a realistic simulation of the detector behaviour after irradiation through a detailed program of validation using Run~2 data. For Run~3 it is adopted as the default digitizer for the planar sensors in the IBL and barrel layers for the production of MC samples.
 %The algorithm is rather time consuming so it has been decided to avoid its usage for IBL 3D sensors and in disks as the algorithm is more complex - hence slower - for the former, and less important for the latter as the level of radiation damage is small so it can be neglected.
 
 As the radiation damage fluence is monotonic  with integrated luminosity, the predictions on electric field and all other derived quantities have to be updated as the LHC run progresses. It has been decided to prepare benchmarks corresponding to the midpoint of each data taking year. For Run3 there are four different conditions in terms of fluence, bias voltage, temperature and so on, leading to different predictions for the electric field and the derived quantities.

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.65\textwidth]{figures/IBLCCEVsIntLum-MCRad-Run2-Run3.pdf}
   \caption{\label{fig:IBL_CCE} Charge collection efficiency as a function of integrated luminosity for IBL planar sensors for data and the ATLAS radiation damage simulation from the beginning of Run~2. The point represent the data and the bands the simulation predictions. The parametric uncertainty on the simulation defining the width of the bands includes variations in the radiation damage model parameters as well as the uncertainty in the luminosity-to-fluence conversion. Horizontal error bars on the data points due to the luminosity uncertainty are smaller than the size of the markers. In Run~2 the bias voltage was increased from 80~V in 2015 to 400~V in 2018. Predictions for Run~3 are shown for bias voltages ranging from 450~V in 2022 to 1000~V in 2025.} 
 \end{center}                 
\end{figure}   
The IBL charge collection efficiency variation as a function of the integrated luminosity measured in data and predicted by the radiation damage from the start of Run~2 is presented in Figure~\ref{fig:IBL_CCE}. This is obtained by computing the ratio of the most probable value of the cluster charge to that measured at the beginning of 2015 in IBL planar sensors. The comparison of data and radiation damage simulation shows the good modelling provided by the MC within its parametric uncertainties. Charge collection efficiency has been reduced by more than 20\% on the IBL planar sensors at the end of Run2, despite the increase in bias voltage. The radiation damage simulation is used to make predictions on the evolution of the IBL charge collection efficiency for Run~3 with bias voltage increasing from 450~V, to be used in 2022, up to a maximum viable value of 1000~V. 


   

 
