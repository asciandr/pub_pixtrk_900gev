The upgrades of the readout electronics and optimisation of the operating parameters resulted in a stable and performant pixel data taking to the end of Run 2. 

Several improvements have been deployed in preparation for Run~3.

Following the optimisation strategy adopted for the last year of Run 2, pixel thresholds have been reduced for the start of Run~3. The analog thresholds for IBL have been set to 2000 $e^-$, down from to 2000  $e^{-}$. Those for the B Layer have been modulated along the longitudinal position on the stave. The central section, more exposed to radiation but less affected by occupancy, where particles traverse a smaller path in the active Si thickness, has analog thresholds set at 4300 $e^-$, down from 5000 $e^-$, while the modules covering the larger $\eta$ range have analog thresholds set at 3500 $e^-$, down from 4300 $e^-$. The corresponding increase in occupancy is estimated to be acceptable within the readout bandwidth limitations.

The bias voltage of all the sensor layers have also been raised for the start of Run 3. This allows to gain charge collection efficiency in the regime above depletion voltage where the higher electric field reduces charge trapping effects in the damaged Si lattice.

Finally, the ToT calibration procedure for IBL has been modified in order to avoid the fitting step that may cause biases at low and high charge values. The new calibration procedure directly takes the average of the injected charge values on the pixels of a FE chip corresponding to each ToT calibration point. The number of parameters stored in the database are increased according to the number of ToT values. For this reason, only the IBL can adopt this scheme since the dynamic range of the ToT is at most 16 for FE-I4, while for FE-I3, the dynamic range of ToT is 255. The look-up-tables for charge to ToT conversion are constructed and stored in the databaes. In simulation, the ToT is extracted by linear interpolation of the input charges and rounded as integer value. Figures \ref{fig:PixelCalibration} present one typical example of the chage-to-ToT calibration for IBL and B-layer modules with a response of the injection charge to ToT correspondence as well as their fitting, respectively. For IBL, the fitted curve (that is not used in Run~3) is also overlaid as reference.
\begin{figure}[htb]
  \centering
  \vspace*{-3.5cm}
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/calibIBL.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/calibBL.pdf} \\
  \end{tabular}
  \caption{Typical example of the chage-to-ToT calibration for IBL and B-layer modules with a response of the injection charge to ToT correspondence as well as their fitting, respectively. For IBL, the fitted curve (not used in Run~3) is also overlaid as reference.~\label{fig:PixelCalibration}}
\end{figure}
These changes were carefully tested using cosmic rays in the course of 2021 and early 2022 and their effects on the pixel detector response are now evaluated on the 900~GeV data.

\subsection{Pixel Detector Response}

The pixel response is characterised in terms of the charge collected, the pixel multiplicity and the charge sharing between these pixels in clusters associated to reconstructed particle tracks. Data are compared to simulation predictions obtained with both the radiation damage MC and the constant charge MC discussed above.

Pixel clusters associated to selected particle tracks are used in this study. In comparing data to simulation, MC tracks are reweighted to match the distribution of the particle incidence angle $\alpha$ on the silicon surface observed for data.

The charge collected in pixel clusters is the first measure of the detector response and its sensitivity to radiation damage. Figure~\ref{fig:PixelCharge} shows the distribution of the cluster charge for the two innermost pixel layers, the IBL and B Layer. The two MC samples show the effect of radiation damage and the good description of data obtained by the radiation damage simulation.   

\begin{figure}[htb]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLClusterCharge.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.BLClusterCharge.pdf} \\
  \end{tabular}
  \caption{Cluster charge corrected by the particle path in the Si for IBL (left panel) and B layer (right panel) planar sensors. Points represent data, the histogram the MC predictions obtained with radiation damage (continuous line histogram) and constant charge (dashed line histogram) simulation~\label{fig:PixelCharge}}
\end{figure}

The second measure of the pixel detector response is the distribution of the number of pixels in clusters associated to tracks. This distribution depends on the incidence angle of the particle  and the position of impact of the particle on the detector plane but also to the charge diffusion and the pixel thresholds. The distributions in the projection transverse ($r-\phi$ or local $X$) and longitudinal ($z$ or local $Y$) to the beam axis are shown in Figure~\ref{fig:PixelNPix} for the IBL and the B Layer.

\begin{figure}[htb]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLNPixX.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.BLNPixX.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLNPixY.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.BLNPixY.pdf} \\    
  \end{tabular}
  \caption{Distributions of the number of pixels in clusters in the projections transverse ($r-\phi$ or local $X$) and longitudinal ($z$ or local $Y$) to the beam axis for the IBL (left panels) and B Layer (right panels) planar sensors. Points represent data, the histogram the MC predictions obtained with radiation damage (continuous line histogram) and constant charge (dashed line histogram) simulation~\label{fig:PixelNPix}}
\end{figure}

\subsection{Pixel Hit-on-Track rates and spatial resolution}

The efficiency and spatial resolution are the two performance figures most important for particle track reconstruction. Appropriate changes in pixel operation parameters have so far ensured a rather uniform performance through Run~2. This will also be the case for the start of Run~3.

The fraction of particle tracks reconstructed with associated pixel detector hits in a given layer gives an indication of the detector efficiency for tracking purposes, although its definition differs from that of hit efficiency, due to the contributions from module overlaps and particles not traversing the layer under study as well as disabled modules.  This fraction is computed for the two innermost layers and compared to the simulation predictions. For this study, reconstructed tracks are required to have hits on the two layers closest to the layer under study, i.e.\ on the B Layer and Layer 1 when studying the rate on the IBL and on the IBL and Layer 1 when studying that on the B Layer. This requirement reduces the rate of tracks originating at a radial position larger than that of the layer under study or with a poor extrapolation on it. 

The fraction of tracks with IBL and B Layer hits-on-track is studied as a function of the pseudorapidity, $\eta$, of the track. Tracks with small azimuthal incidence angles on the pixel layers deposit on average less charge per pixel than those at larger angles. This increases the probability that the collected charge is below the pixel threshold resulting in an inefficiency. 

\begin{figure}[htb]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.TrkPixFracVsEtaMC.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.TrkPixFracVsEtaYears.pdf} \\
  \end{tabular}
  \caption{Average fraction of tracks with an associated hit in the IBL and B Layer as a function of the track pseudorapidity, $\eta$, comparing the Run~3 900~GeV data to MC (left) and the Run~2 data (left). The fraction is computed for fraction of selected tracks with $p_{T} >$ 1.0~GeV, $|\eta| <$ 2.1 and pixel hits in the two neighbouring layers having an associated hit in the layer under study. \label{fig:PixelHitOnTrackEta}}
\end{figure}

The distributions obtained for the IBL and B Layer are shown in Figure~\ref{fig:PixelHitOnTrackEta}. The track fractions with IBL and B layer hits integrated over $\eta$ observed in data and predicted by simulation are summarised in Table~\ref{tab:PixelHitOnTrack} where they are compared to the corresponding fractions measured at the beginning and at the end of  Run~2 in 2015 and 2018, respectively. The track fractions integrated over $\eta$ for the MC and the 13~TeV Run~2 data samples are computed by reweighting tracks on $\eta$ to reproduce the pseudorapidity distribution of the Run~3 900~GeV data. The 900~GeV agrees well with the MC predictions. The higher efficiency registered on data for the B Layer at central track is due to the ``hybrid'' threshold setting adopted in Run~3 that is not yet included in the MC. A comparison of the predictions from the two MC samples shows that radiation damage effect on the efficiency for associating pixel hits to tracks is small for the fluences integrated so far.

\begin{table}[h!]
  \caption{Average fraction of tracks with associated hits in the two innermost pixel layers integrated over $\eta$ measured on 900~GeV collision data at the start of Run 3 compared to predictions from simulation with and without radiation damage effects and to the corresponding values measured on 13~TeV collisions data at the beginning and at the end of Run~2. MC and 13~TeV Run~2 data are rescaled to the same $\eta$ distribution of the Run~3 900~GeV data. No correction for the effect of disabled module is applied. \label{tab:PixelHitOnTrack}}
  \centering
  \begin{tabular}{|l|c|c|}
    \hline
    Data set       & IBL             & B Layer  \\
    \hline \hline
    Start of Run~3 &                 &         \\
    900~GeV        & 0.971$\pm$0.003 & 0.950$\pm$0.003 \\
    Data           &                 &         \\ \hline
    Start of Run~3 &                 &         \\
    900~GeV        & 0.970$\pm$0.002 & 0.945$\pm$0.003 \\
    MC Rad. Damage &                 &         \\ \hline 
    Start of Run~3 &                 &         \\
    900~GeV        & 0.974$\pm$0.002 & 0.951$\pm$0.003 \\
    MC Const. Charge&                &         \\ \hline       
    Early Run~2    &                 &         \\
    13~TeV         & 0.973$\pm$0.002 & 0.958$\pm$0.002 \\
    Data           &                 &         \\ \hline
    Late Run~2     &                 &         \\
    13~TeV         & 0.958$\pm$0.002 & 0.914$\pm$0.002 \\
    Data           &                 &         \\ \hline        
  \end{tabular}
\end{table}

This analysis indicates that the fraction of tracks with hits in the two innermost pixel layers at the start of Run 3 are higher than those registered at the end of 2018.  This performance improvement owes to the optimisation of the pixel operating conditions, with the increase of the bias voltage and the reduction of the analog and ToT thresholds, and to the reduction of the number of inactive modules after the LS2 intervention.

The second performance figure studied on the 900~GeV data is the IBL spatial resolution.  Owing to its position closest to the interaction region and its smaller pixel pitch along $z$ compared to the sensors equipping the other layers, the IBL carries the highest weight in determining the extrapolation of tracks to the primary vertex for particles of low-to-moderate transverse momentum.

The IBL spatial resolution is studied using the active region of adjacent module overlaps in $\phi$, that are approximately 1.5 \si{\mm} wide, using a technique already exploited by ATLAS~\cite{ATL-INDET-PUB-2016-001}.
In these regions particles traversing the IBL layers leave hits on the two neighbouring modules. The spatial resolution can be extracted from the distribution of the corrected difference $\Delta_{r-\phi}$ and $\Delta_{z}$ of the transverse and longitudinal positions of the two hits, respectively. The corrections applied to the $\Delta$ values account for the geometrical effects due to the different radial position of the modules traversed by the particle track.

The width of the $\Delta$ distribution is $\sqrt{2}$ times larger than the hit resolution we intend to measure, under the assumption that the two hits have the same resolution. This method has the advantage of depending only weakly on the reconstructed track parameters and their uncertainties and on detector alignmnent. The overlap technique for the determination of the pixel spatial resolution has been validated using simulation.

For this study tighter selection criteria are applied. First, only tracks having two associated hits in the IBL, within module overlap fiducial region defined along $\phi$, at least two hits on the other pixel layers and at least six in the sum of Pixel and SCT hits are considered. Clusters reconstructed  at the edge of a module, where the charge interpolation may be inefficient, are not considered. In addition, the track transverse momentum cut is raised to \pt $>$ 3.0 \si{\GeV} to reduce multiple scattering effects.

A total of $\sim$2400~ reconstructed tracks with two hits in the IBL overlap regions has been selected. 
\begin{figure}[htb]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLResDeltaRPhi.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLResDeltaZ.pdf} \\
  \end{tabular}
  \caption{Difference between the corrected positions of reconstructed IBL clusters in the $r - \phi$ (left) and $z$ (right) projections. The distributions are obtained from samples of reconstructed particle tracks traversing the overlapping IBL modules and having two IBL hits associated, $|\eta| <$ 2 for $r - \phi$ and $<$ 1 for $z$, in 900~GeV collision events (filled circles with errors bars) and simulation (histogram). The curves represent the Gaussian fit to data.
    %IBL spatial resolution along $z$. (Left) difference between the corrected positions of reconstructed IBL clusters in the z projection. The distributions are obtained from samples of selected reconstructed particle tracks traversing the overlapping IBL modules and having two IBL hits associated. The curve represent the Gaussian fit to data. (Right) measured IBL hit resolution of the track $|\eta|$, in collision data (filled circles) and simulation (open circles).
    \label{fig:PixelResDelta}}
\end{figure}

The resolution $\sigma_{\mathrm{point}} = \sigma_{\mathrm{\Delta}}/\sqrt{2}$, where $\sigma_{\mathrm{\Delta}}$ is the fitted Gaussian width of the $\Delta$ distributions shown in Figure~\ref{fig:PixelResDelta}. The dependance of the measured spatial resolution on the variables most relevant to the two projections, the number of pixels in the cluster for $r - \phi$ and the track azimuthal incidence angle for $z$, is shown in Figure~\ref{fig:PixelRes} for data and simulation.

\begin{figure}[htb]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLResRPhiZVsNPixX.pdf} &
    \includegraphics[width=0.45\textwidth]{figures/data22_900GeV.IBLResZVsTheta.pdf} \\
  \end{tabular}
  \caption{Measured IBL hit resolution in the $r - \phi$ (left) and $z$ (right) projections as a function of the number of cluster width along $\phi$ and the track $|\eta|$, respectively in selected $Z+\mathrm{jets}$, $Z \rightarrow \mu \mu$ events in 900 GeV collision events (filled circles with errors bars) and simulation (open circles).
    \label{fig:PixelRes}}
\end{figure}

The pixel detector spatial resolution evolves with the reduction in collected charge due to radiation damage effects. However, their impact on pixel spatial resolution is rather small because the MDN determining the pixel hit position is based on relative quantities, such as fraction of cluster charge shared between neighbouring pixels. This degradation is modelled by the radiation damage MC that predicts a $\simeq$25\% increase from the start of Run~2 to the end of Run~3. This degradation is mitigated by training the network on radiation damage MC samples and by the improvement in the spatial resolution of pixel hits obtained with the MDN for Run~3. The spatial resolution estimated on the 900~GeV data agrees with the radiation damage simulation predictions. Constant charge simulation yields spatial resolution values that are lower by $\simeq$5\% in both coordinates, compared to those predictions.



